package demo.com;

import java.util.ArrayList;

public class MinMax {
	private int min;
	private int max;
	// Standard setters and getters
	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}
//Parameterized constructor
	public MinMax(int min, int max) {
		super();
		this.min = min;
		this.max = max;
	}
	//default constructor
	public MinMax() {
		this.max=0;
		this.min=0;
	}

//Method for finding minimum element from dynamic array
	public void getMinElement(ArrayList<Integer>arr,int size){
		int element=0;
		Integer min;
		if(size==1){
			min=arr.get(0);
			System.out.println(min);
		}
		else{
			min=arr.get(0);
			for(int i=1;i<size;i++){
				
				if(min<arr.get(i)){
					 element=min;
					
				}
				else{
					min=arr.get(i);
					element=min;
				}
				
				
				}
			//print minimum element from dynamic array
			 System.out.println("Minimum element from array:"+" "+element);
			}
		
		
	}
	
	//Method for finding maximum element from dynamic array
	public void getMaxElement(ArrayList<Integer>arr,int size){
		
		Integer max;
		if(size==1){
			max=arr.get(0);
			
		}
		else{
			max=arr.get(0);
			for(int i=1;i<size;i++){
				if(arr.get(i)>max){
					 max=arr.get(i);
				}
				
			}
			//print maximum element from  dynamic array
			System.out.println("Maximum element from array:"+" "+max);
		}
		
}
	
	

}
