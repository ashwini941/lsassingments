package demo.com;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try{
		System.out.println("Enter size of array");
		@SuppressWarnings("resource")
		//scanner for accept array size from user
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		
		
		
		//dynamic array to store the array elements
	
		ArrayList<Integer> arr=new ArrayList<Integer>();
		
		System.out.println("Enter element of array");
		for(int i=0;i<n;i++){
			
			@SuppressWarnings("resource")
			//scanner for accept array elements from user
			
			Scanner m=new Scanner(System.in);
			int k=m.nextInt();
			arr.add(k);

		}
		
		int e=arr.size();
		//printing array elements
		System.out.println("Elements of array:");	
		for(Integer t :arr){
			System.out.println(t);	
		}
		
		
		//create object of class 
		MinMax m1=new MinMax();
		int size=e;
		//calling method for finding minimum elements
		m1.getMinElement(arr, size);
		//calling method for finding maximum elements
		m1.getMaxElement(arr, size);
		}
		
		catch(Exception e){
			System.out.println("please enter interger number for  array");
		}
		
	}

}
