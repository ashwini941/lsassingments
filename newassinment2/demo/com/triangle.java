package demo.com;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.util.Scanner;

import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class triangle extends Applet {
	
	/**
	 * 
	 */

	public void init() {

		setBackground(Color.pink); //set applet viewer background color
		setSize(500, 500); //set applet viewer size

	}

	public void paint(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;
		g2d.setPaint(Color.blue);
		try{
		 String str1 = JOptionPane.showInputDialog("Enter side1");				//accepting triangle sides from user
		 String str2 = JOptionPane.showInputDialog("Enter side2");
		 String str3 = JOptionPane.showInputDialog("Enter side3");
		
		 double side1=Double.parseDouble(str1);
		 double side2=Double.parseDouble(str2);							//convert string value into double
		 double side3=Double.parseDouble(str3);
		 
		g2d.draw(new Line2D.Double(20, (side1 + 100), (20 + (side3 / 2)), 100));	
		g2d.draw(new Line2D.Double((20 + side3), (side2 + 100), (20 + (side3 / 2)), 100)); 
		g2d.draw(new Line2D.Double(20, (side1 + 100), (side3 + 20), (side2 + 100)));

		if ((side1 == side2) && (side2 == side3)) {
			g2d.drawString("Euilateral triangle", 50, 50);

		} else if ((side1 == side2) || (side2 == side3) || (side1 == side3)) {
			g2d.drawString("Isoscele triangle", 50, 50);

		} else {
			g2d.drawString("Scalene triangle", 50, 50);
		}
		}catch(NumberFormatException e){
			JOptionPane.showMessageDialog(null, "please enter correct sides");;
			
		}

	}

	public static void main(String s[]) {

		//JFrame frame = new JFrame("Show Triangle");
		Applet applet = new triangle();
		//frame.getContentPane().add("Center", applet);
		applet.init();
		//frame.setSize(300, 250);
		//frame.setVisible(true);
		
	}

}
